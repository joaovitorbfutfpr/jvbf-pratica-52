
import utfpr.ct.dainf.if62c.pratica.CoeficienteZeroException;
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;
import utfpr.ct.dainf.if62c.pratica.SemSolucaoRealException;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {
    public static void main(String[] args) {
        try{
            Equacao2Grau<Integer> eq1 = new Equacao2Grau<>(1,-3,-10);
            Equacao2Grau<Integer> eq2 = new Equacao2Grau<>(9,-12,4);
            Equacao2Grau<Integer> eq3 = new Equacao2Grau<>(3,3,5);
            System.out.println("Equacao DIFERENTE a1 b-3 c-10: RaizPos: "+eq1.getRaiz1()
                    +" RaizNeg: "+eq1.getRaiz2());
            System.out.println("Equacao IGUAL a9 b-12 c4: RaizPos: "+eq2.getRaiz1()
                    +" RaizNeg: "+eq2.getRaiz2());
            System.out.println("Equacao SEM RAIZ a3 b3 c5: RaizPos: "+eq3.getRaiz1()
                    +" RaizNeg: "+eq3.getRaiz2());
        } catch (CoeficienteZeroException coe) {
            System.out.println("Ocorreu um erro de Coeficiente a Zero: "
                + coe.getLocalizedMessage());
        } catch (SemSolucaoRealException ssre) {
            System.out.println("Ocorreu um erro de Sem Solucao Real: "
                + ssre.getLocalizedMessage());
        }
    }
}
