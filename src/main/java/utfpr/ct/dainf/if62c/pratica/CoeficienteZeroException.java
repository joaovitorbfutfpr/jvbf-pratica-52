/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author JoaoVitorBF
 */
public class CoeficienteZeroException extends RuntimeException {

    public CoeficienteZeroException() {
        super("Coeficiente a não pode ser zero");
    }
    
}
