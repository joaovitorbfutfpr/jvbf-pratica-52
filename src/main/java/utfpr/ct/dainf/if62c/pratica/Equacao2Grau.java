/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author JoaoVitorBF
 * @param <T>
 */
public class Equacao2Grau<T extends Number> {
    T a, b, c;

    public Equacao2Grau(T a, T b, T c) {
        if (a.intValue() == 0) {
            throw new CoeficienteZeroException();
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public T getA() {
        return a;
    }

    public void setA(T a) {
        if (a.intValue() == 0) {
            throw new CoeficienteZeroException();
        }
        this.a = a;
    }

    public T getB() {
        return b;
    }

    public void setB(T b) {
        this.b = b;
    }

    public T getC() {
        return c;
    }

    public void setC(T c) {
        this.c = c;
    }
    
    public double getRaiz1() {
        double result = Math.pow(b.doubleValue(), 2)-(4*a.doubleValue()*c.doubleValue());
        if (result < 0) {
            throw new SemSolucaoRealException();
        }
        result =((b.doubleValue()*-1)+Math.sqrt(result))/2*a.doubleValue();
        return result;
    }
    
    public double getRaiz2() {
        double result = Math.pow(b.doubleValue(), 2)-(4*a.doubleValue()*c.doubleValue());
        if (result < 0) {
            throw new SemSolucaoRealException();
        }
        result =((b.doubleValue()*-1)-Math.sqrt(result))/2*a.doubleValue();
        return result;
    }
    
}
